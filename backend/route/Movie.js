const{query}= require("express");
const express = require("express");
const router =express.Router();
const utils =require("../utils");



// display all data of movie
router.get("/:movie_title", (request, response) => {
    const {movie_title } = request.params;
  
    const connection = utils.openConnection();
  
    const statement = `
          select * from Movie where
          movie_title = '${movie_title}'
        `;
    connection.query(statement, (error, result) => {
      //connection.end();
      if (result.length > 0) {
        console.log(result.length);
        console.log(result);
        response.send(utils.createResult(error, result));
      } else {
        response.send("Movie not found !");
      }
    });
  });
  //add movie
  router.post("/add", (request, response) => {
    const { movie_id, movie_title, movie_release_date,movie_time,director_name } = request.body;
  
    const connection = utils.openConnection();
    console.log(connection);
    const statement = `
          insert into Movie
            ( movie_id, movie_title, movie_release_date, movie_time, director_name)
          values
            ( ${movie_id},'${movie_title}','${movie_release_date}',${movie_time},'${director_name}')
        `;
    connection.query(statement, (error, result) => {
      //connection.end();
      response.send(utils.createResult(error, result));
    });
  });
  
  router.put("/update/:movie_title", (request, response) => {
    const { movie_title  } = request.params;
    const { movie_release_date , movie_time } = request.body;
  
    const statement = `
      update Movie
      set
      movie_release_date =${movie_release_date}
      movie_time =${movie_time}

      where
      movie_title = '${movie_title}'
    `;
    const connection = utils.openConnection();
    connection.query(statement, (error, result) => {
     // connection.end();
      console.log(statement);
  
      response.send(utils.createResult(error, result));
    });
  });
  router.delete("/remove/:movie_title", (request, response) => {
    const { movie_title  } = request.params;
    const statement = `
      delete from Movie
      where
      movie_title = '${movie_title}'
    `;
    const connection = utils.openConnection();
    connection.query(statement, (error, result) => {
      // connection.end();
      console.log(statement);
      response.send(utils.createResult(error, result));
    });
  });
  module.exports = router;
  
  