const express = require("express");

const app = express();

const cors = require("cors");
const routerMovie = require("./route/Movie");
app.use(cors("*"));
app.use(express.json());
app.use("/Movie", routerMovie);

app.listen(4000, "0.0.0.0", () => {
  console.log("server started on port 4000");
});
