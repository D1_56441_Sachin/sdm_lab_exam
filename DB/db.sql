//create Movie(movie_id, movie_title, movie_release_date,movie_time,director_name)
CREATE TABLE Movie(
movie_id int, 
movie_title varchar(200),
movie_release_date Date,
movie_time time,
director_name varchar(100))

INSERT INTO Movie (movie_id, movie_title, movie_release_date,movie_time,director_name)
VALUES(1, "Sholay", '1/7/1975', 09:30:00, "RD");
VALUES(2, "singham", '2025-6-14', 01:00:00, "ajay");
VALUES(3, "DDLJ", '1/7/1995', 01:30:00, "dds");
